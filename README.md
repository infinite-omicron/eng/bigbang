# BigBang

_BigBang development instance for testing._

## Usage

```
gpg --batch --full-generate-key --rfc4880 --digest-algo sha512 --cert-digest-algo sha512 <<EOF
    %no-protection
    # %no-protection: means the private key won't be password protected
    # (no password is a fluxcd requirement, it might also be true for argo & sops)
    Key-Type: RSA
    Key-Length: 4096
    Subkey-Type: RSA
    Subkey-Length: 4096
    Expire-Date: 0
    Name-Real: infinite-omicron-eng-bigbang
    Name-Comment: infinite-omicron-eng-bigbang
EOF
```

```
gpg_private_key=$(gpg --export-secret-keys --armor infinite-omicron-eng-bigbang)
gpg_public_key=$(gpg --export --armor infinite-omicron-eng-bigbang)
gpg_fingerprint=$(gpg --list-keys --fingerprint | grep "infinite-omicron-eng-bigbang" -B 1 | grep -v "infinite-omicron-eng-bigbang" | tr -d ' ' | tr -d 'Keyfingerprint=')
```

```
flux bootstrap git \
  --url=https://gitlab.com/infinite-omicron/eng/bigbang.git \
  --branch=main \
  --path=dev \
  --username=$gitlab_username \
  --password=$gitlab_password \
   --token-auth=true
```

## Reference

- https://repo1.dso.mil/platform-one/big-bang/customers/template/-/releases/1.12.0

